package main;

import service.UserService;
import util.DB;
import util.Util;

public class Main {
	
	public UserService us = new UserService();

	public static void main(String[] args) {
		new Main().init();
	}
	
	public void init(){
		String returnValue = Util.scance("您好，欢迎使用学生管理系统，请输入1：登陆，2：注册");	
		if("1".equals(returnValue)){//登陆
			us.login();
		}else if("2".equals(returnValue)){
		    us.register();
		}
	}
}
