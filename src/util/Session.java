package util;

import entity.Student;
import entity.User;

public class Session {

	private static User user;
	
	private static Student student; 

	public static User getUser() {
		return user;
	}

	public static void setUser(User user) {
		Session.user = user;
	}

	public static Student getStudent() {
		return student;
	}

	public static void setStudent(Student student) {
		Session.student = student;
	}
	
	
}
