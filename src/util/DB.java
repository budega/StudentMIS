package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * MYSQL链接工具类
 * @author kele
 *
 */

public class DB {

	private static final String URL = "jdbc:mysql://127.0.0.1:3306/student?Unicode=true&characterEncoding=utf-8"; //MYSQL链接地址
	
	private static final String USER_NAME = "root"; //MYSQL数据库用户名
	
	private static final String PASSWORD = "root"; //MYSQL数据库密码
	
	/**
	 * 获取链接
	 * @return
	 */
	public static Connection getConnection(){
		Connection conn = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(URL,USER_NAME,PASSWORD);			
		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
	
	/**
	 * 关闭数据库链接
	 * @param conn
	 */
	public static void closeConnection(Connection conn){
		if(conn!=null){
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{
				if(conn!=null){
					conn = null;
				}
			}
		}
	}
	
	/**
	 * 通过sql语句查询数据,并返回数组
	 * @param sql
	 */
	public static List query(String sql){
		List<List> data = new ArrayList<List>();
		Connection conn = DB.getConnection();
		ResultSet rs = null;
		PreparedStatement pstmt;
		try {
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			ResultSetMetaData rsd = pstmt.getMetaData();
			while(rs.next()){
				List<String> list = new ArrayList<String>();
				for(int i=1;i<rsd.getColumnCount()+1;i++){
					list.add(rs.getString(i));
				}
				data.add(list);
			}
			return data;
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return null;
	}
	
	/**
	 * 执行更新SQL方法
	 * @param sql
	 * @return
	 */
	public static boolean run(String sql){
		Connection conn = DB.getConnection();		
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	
	
}
