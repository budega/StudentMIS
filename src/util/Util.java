package util;

import java.util.Scanner;

public class Util {
	

	/**
	 * 控制台输入，并获取输入结果
	 * @param str
	 * @return
	 */
	public static String  scance(String str){		
		System.out.println(str);
		Scanner sc = new Scanner(System.in);
		String returnValue = sc.next();
		if("bye".equals(returnValue) || "exit".equals(returnValue)){			
			Util.out("感谢您使用本系统,欢迎下次管理!");
			System.exit(0);
		}
		return returnValue;
	}
	
	/**
	 * 控制台打印信息
	 * @param str
	 */
	public static void  out(String str){
		System.out.println(str);
	}
}
