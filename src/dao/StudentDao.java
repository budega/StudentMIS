package dao;

import java.util.List;

import util.DB;
import util.Session;
import util.Util;
import entity.Student;
import entity.User;

public class StudentDao {
	
	public Student getStudent(){
		String sql = "SELECT ID,NAME,C_ID,U_ID FROM STUDENT T WHERE T.U_ID='"+Session.getUser().getId()+"'";
		List<List<String>> data = DB.query(sql);
		Student s = null;
		if(data.size()>0){
			s = new Student();
			s.setId(data.get(0).get(0));
			s.setName(data.get(0).get(1));
			s.setCid(data.get(0).get(2));
			s.setUid(data.get(0).get(3));
		}
		
		return s;
	}
	
	/**
	 * 查询个人信息
	 * @return
	 */
	public List<List<String>> showInfo(){	
		User u = Session.getUser();
		if(u!=null){
			String sql = "select U.ID,U.USER_NAME,S.NAME,C.CLASS_NAME from USER U ,CLASS C ,STUDENT S WHERE U.ID = S.U_ID AND S.C_ID = C.ID AND U.ID='"+u.getId()+"'";
			return DB.query(sql);
		}
		
		return null;
	}
	
	/**
	 * 查询课程表
	 * @return
	 */
	public List<List<String>> showCourse(){
		User u = Session.getUser();
		if(u!=null){
			String sql = "SELECT C.COURSE FROM STUDENT S ,CLASS C WHERE S.C_ID = C.ID AND S.U_ID='"+u.getId()+"'";
			return DB.query(sql);
		}
		return null;
	}
	
	/**
	 * 查询公告
	 * @return
	 */
	public List<List<String>> showNotic(){
		User u = Session.getUser();
		if(u!=null){
			String sql = "SELECT C.NOTICE FROM STUDENT S ,CLASS C WHERE S.C_ID = C.ID AND S.U_ID='"+u.getId()+"'";
			return DB.query(sql);
		}
		return null;
	}
	
	/**
	 * 查询班级留言
	 * @return
	 */
	public List<List<String>> showClassMsg(){
		User u = Session.getUser();
		Student s = Session.getStudent();
		if(u!=null){
			String sql = "select S.NAME,C.CLASS_NAME,M.MESSAGE from message M ,STUDENT S,CLASS C WHERE M.S_ID=S.ID AND M.C_ID = C.ID AND M.C_ID ='"+s.getCid()+"'";
			return DB.query(sql);
		}
		return null;
	} 
	
	/**
	 * 为班级留言
	 * @param msg
	 * @return
	 */
	public boolean sendMsg(String msg){
		User u = Session.getUser();
		Student s = Session.getStudent();
		String sql = "INSERT INTO message (U_ID,S_ID,C_ID,MESSAGE) VALUES ('"+u.getId()+"','"+s.getId()+"','"+s.getCid()+"','"+msg+"')";		
		return DB.run(sql);
	}
	
	public boolean modifyInfo(String name,String myClass){
		User u = Session.getUser();
		String sql = "INSERT INTO STUDENT (NAME,C_ID,U_ID) VALUE ('"+name+"','"+myClass+"','"+u.getId()+"')";		
		return DB.run(sql);
	}
	
}
