package dao;

import java.util.List;

import util.DB;

public class ClassDao {

	/**
	 * 查询所有班级
	 * @return
	 */
	public List queryClass(){
		String sql = "SELECT * FROM CLASS";
		List<List<String>> data = DB.query(sql);
		return data;
	}
	
	/**
	 * 通过ID查询班级
	 * @param id
	 * @return
	 */
	public List queryClassById(String id){
		String sql = "SELECT * FROM CLASS WHERE ID='"+id+"'";
		return DB.query(sql);
	}
	
	/**
	 * 通过className查询班级
	 * @param id
	 * @return
	 */
	public List<List<String>> queryClassByName(String name){
		String sql = "SELECT * FROM CLASS WHERE CLASS_NAME='"+name+"'";
		return DB.query(sql);
	}
	
	public boolean addClass(String className,String notice,String course){
		String sql = "INSERT INTO CLASS (CLASS_NAME,NOTICE,COURSE) VALUES ('"+className+"','"+notice+"','"+course+"')";
		return DB.run(sql);
	}
	
	/**
	 * 删除班级
	 * @param className
	 * @return
	 */
	public boolean delClass(String className){
		List<List<String>> list = this.queryClassByName(className);
		if(!list.isEmpty()){
			String sql = "DELETE FROM CLASS WHERE ID='"+list.get(0).get(0)+"'";
			return DB.run(sql);
		}
		return false;
		
		
	}
	
}
