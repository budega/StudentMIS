package dao;

import java.util.List;

import entity.User;
import util.DB;
import util.Util;

public class UserDao {	
	
	public User login(String userName,String password){
		
		String sql = "SELECT * FROM USER WHERE USER_NAME='"+userName+"' and PASSWORD='"+password+"'";
		List<List<String>> data = DB.query(sql);
		if(data.size()>0){
			User user = new User();
			List<String> list = data.get(0);
			user.setId(Integer.parseInt(list.get(0)));
			user.setUserName(list.get(1));
			user.setPassword(list.get(2));
			user.setRegDate(list.get(3));
			user.setUserType(list.get(4));
			return user;
		}
		return null;
	}
	
	/**
	 * 查询用户是否存在
	 * @return
	 */
	public boolean isExist(String userName){
		String sql = "SELECT * FROM USER WHERE USER_NAME='"+userName+"'";
		List<List<String>> data = DB.query(sql);
		if(data.size()>0){
			return true;
		}
		return false;
	}

	public boolean register(String userName, String password) {
		String sql = "INSERT INTO USER (USER_NAME,PASSWORD,REG_DATE,USER_TYPE) VALUES ('"+userName+"', '"+password+"',now(),0)";
		if(DB.run(sql)){
			return true;
		}
		return false;
	}
}
