package service;

import java.util.List;

import dao.ClassDao;
import dao.StudentDao;
import entity.Student;
import util.Session;
import util.Util;

/**
 * 学生Service
 * @author kele
 *
 */
public class StudentService {
	
	private StudentDao studentDao = new StudentDao();	
	
	private ClassDao classDao = new ClassDao();
	
	public void init(){
		Session.setStudent(studentDao.getStudent());
		if(Session.getStudent() == null){
			this.modifyInfo();
		}else{
			Util.out("1:查看个人信息");
			Util.out("2:课表查询");
			Util.out("3:查看班级公告");
			Util.out("4:查询班级留言");
			Util.out("5:班级留言");
			String returnValue = Util.scance("请输入你想要做的事情的代号:");
			if("1".equals(returnValue)){
				this.showInfo();
			}else if("2".equals(returnValue)){
				this.showCourse();
			}else if("3".equals(returnValue)){
				this.showClassNotic();
			}else if("4".equals(returnValue)){
				this.showClassMsg();
			}else if("5".equals(returnValue)){
				this.sendMsg();
			}
		}
		 
		
		this.init();
		
		
	}
	
	public void modifyInfo(){
		String name = Util.scance("请输入你的姓名:");
		List<List<String>> data = classDao.queryClass();
		Util.out("现在下列可选的班级代号");
		if(data!=null && data.size()>0){
			for(int i=0;i<data.size();i++){
				Util.out(i+":"+data.get(i).get(1));
			}
		}
		String myClass = Util.scance("请输入您的班级代号：");
		try{
			studentDao.modifyInfo(name, data.get(Integer.parseInt(myClass)).get(0));
		}catch(Exception e){
			Util.out("你在逗我玩吗？");
		}
		
	}

	/**
	 * 查看个人信息
	 */
	public void showInfo(){
		List<List<String>> data = studentDao.showInfo();
		if(data!=null && data.size()>0){			
			Util.out("您的信息：\n");
			Util.out("用户名："+data.get(0).get(1));
			Util.out("姓名："+data.get(0).get(2));
			Util.out("班级："+data.get(0).get(3));
		}
	}
	
	/**
	 * 查看课程
	 */
	public void showCourse(){
		List<List<String>> data = studentDao.showCourse();
		if(data.size()>0){
			Util.out("您的班级课程："+data.get(0).get(0));
		}else{
			Util.out("你们班级没有课程！");
		}
	}
	
	/**
	 * 查看班级公告
	 */
	public void showClassNotic(){
		List<List<String>> data = studentDao.showNotic();
		if(data.size()>0){
			Util.out("您的班级公告是："+data.get(0).get(0));
		}else{
			Util.out("您的没有编辑公告！");
		}
	}
	
	/**
	 * 查询班级留言
	 */
	public void showClassMsg(){
		List<List<String>> data = studentDao.showClassMsg();
		if(data!=null && data.size() >0){
			for(List list:data){
				Util.out(list.get(0)+"@"+list.get(1)+":"+list.get(2));
			}
		}
	}
	
	/**
	 * 班级留言
	 */
	public void sendMsg(){
		String returnValue = Util.scance("请为你的班级留言");
		if(studentDao.sendMsg(returnValue)){
			Util.out("留言成功！");
		}else{
			Util.out("留言失败！");
		}
	}
	
	
}
