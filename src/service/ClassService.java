package service;

import java.util.List;

import util.Util;
import dao.ClassDao;

public class ClassService {

	public ClassDao classDao = new ClassDao();
	
	/**
	 * 班级service初始化
	 */
	public void init(){
		Util.out("1:查询班级");
		Util.out("2:添加班级");
		Util.out("3:删除班级");
		Util.out("4:退出");
		String returnValue = Util.scance("请输入你想要做的事情的代号:");
		if("1".equals(returnValue)){
			this.query();
		}else if("2".equals(returnValue)){
			this.add();
		}else if("3".equals(returnValue)){
			this.delete();
		}else if("4".equals(returnValue)){
			Util.out("感谢使用");
			System.exit(0);
		}
		this.init();
	}
	
	/**
	 * 查询班级
	 * @return
	 */
	public void query(){
		List<List<String>> list = classDao.queryClass();
		if(list.size()>0){
			for(int i=0;i<list.size();i++){
				Util.out(list.get(i).get(1));
			}
		}else{
			Util.out("现在没有班级，请添加！");
		}
		
	}
	
	/**
	 * 删除班级
	 */
	public void delete(){
		String returnValue = Util.scance("请输入要删除的班级名称:");
		if(classDao.delClass(returnValue)){
			Util.out("删除成功!");			
		}else{
			Util.out("删除成功!");	
		}	
		
	}
	
	public void add(){
		String className = Util.scance("请输入要添加的班级名称:");
		String classNotice = Util.scance("请输入要添加的公告:");
		String classCourse = Util.scance("请输入要添加的课程:");
		if(classDao.addClass(className,classNotice,classCourse)){
			Util.out("添加成功!");	
		}else{
			Util.out("添加成功!");	
		}		
	}
}
