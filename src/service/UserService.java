package service;

import main.Main;
import dao.UserDao;
import entity.User;
import util.Session;
import util.Util;

public class UserService {
	
	private UserDao userDao = new UserDao();
	private ClassService classService = new ClassService();
	private StudentService studentService = new StudentService();
	
	/**
	 * ClassService初始化
	 */
	public void init(){
		
	}

	/**
	 * 用户登陆方法
	 * @return
	 */
	public void login(){
		boolean flag = false; //是否登陆成功的变量
		String userName = Util.scance("请输入用户名:");
		String password = Util.scance("请输入密码:");
		User user = userDao.login(userName, password);
		if(user!=null){
			Session.setUser(user);
			if("1".equals(user.getUserType())){
				Util.out("登陆成功,欢迎"+user.getUserName()+"管理员管理学生管理系统");
				classService.init();
				 //初始化用户
			}else if("0".equals(user.getUserType())){
				Util.out("登陆成功,欢迎"+user.getUserName()+"同学,你现在可以做一下操作");
				studentService.init();
				
			}
			
			
		}else{
			Util.out("用户名密码错误");
			new Main().init();
		}
	}
	
	/**
	 * 注册方法
	 * @return
	 */
	public void register(){
		String userName = Util.scance("请输入用户名:");
		
		if(!userDao.isExist(userName)){ 
			String password = Util.scance("请输入密码:");
			boolean flag = userDao.register(userName,password);
			if(flag){
				Util.out("注册成功!");
				new Main().init();
			}else{
				Util.out("注册注册失败!");
			}
		}else{
			Util.out("用户已经存在,请换一个用户名吧!");
			new Main().init();
		}
	}
	
	
}
