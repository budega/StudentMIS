package entity;


/**
 *学生实体类 
 * @author kele
 *
 */
public class Student {

	private String id;//ID
	
	private String name;//姓名
	
	private String cid;//所在班级
	
	private String uid;//用户ID

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
	
	
}
